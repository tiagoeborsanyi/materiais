const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {

    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.password2 = !isEmpty(data.password2) ? data.password2 : '';

    if (!Validator.isLength(data.name, {min: 5, max: 30})) {
        errors.name = 'Nome deve conter entre 5 e 30 caracteres';
    }

    if (Validator.isEmpty(data.name)) {
        errors.name = 'Campo nome é obrigatório';
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = 'Campo email é obrigatório';
    }

    if (!Validator.isEmail(data.email)) {
        errors.email = 'Email esta inválido';
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = 'Senha é obrigatória';
    }

    if (!Validator.isLength(data.password, {min: 3, max: 30})) {
        errors.password = 'Senha deve conter entre 5 e 30 caracteres';
    }

    if (Validator.isEmpty(data.password2)) {
        errors.password2 = 'Campo de confirmação de senha obrigatório';
    }

    if (!Validator.equals(data.password, data.password2)) {
        errors.password2 = 'campos de senha estão diferentes';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}