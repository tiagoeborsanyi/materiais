const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data) {

    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';

    if (!Validator.isLength(data.name, {min: 5, max: 30})) {
        errors.name = 'Nome deve conter entre 5 e 30 caracteres';
    }

    if (Validator.isEmpty(data.name)) {
        errors.name = 'Campo nome é obrigatório';
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = 'Campo email é obrigatório';
    }

    if (!Validator.isEmail(data.email)) {
        errors.email = 'Email esta inválido';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}