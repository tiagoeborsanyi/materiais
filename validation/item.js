const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateItensInput(data) {

    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : '';
    data.brand = !isEmpty(data.brand) ? data.brand : '';
    data.specification = !isEmpty(data.specification) ? data.specification : '';
    data.unit = !isEmpty(data.unit) ? data.unit : '';
    data.specificationunit = !isEmpty(data.specificationunit) ? data.specificationunit : '';
    data.qtdstock = !isEmpty(data.qtdstock) ? data.qtdstock : '';
    data.codeitem = !isEmpty(data.codeitem) ? data.codeitem : '';

    if (!Validator.isLength(data.name, {min: 3, max: 70})) {
        errors.name = 'Nome deve conter entre 3 e 70 caracteres';
    }

    if (Validator.isEmpty(data.name)) {
        errors.name = 'Campo nome é obrigatório';
    }

    if (Validator.isEmpty(data.brand)) {
        errors.brand = 'Campo marca é obrigatório';
    }

    if (Validator.isEmpty(data.specification)) {
        errors.specification = 'Campo especificação técnica é obrigatório';
    }

    if (Validator.isEmpty(data.unit)) {
        errors.unit = 'Campo campo unidade é obrigatório';
    }

    if (Validator.isEmpty(data.specificationunit)) {
        errors.specificationunit = 'Campo campo unidade é obrigatório';
    }

    if (Validator.isEmpty(data.qtdstock)) {
        errors.qtdstock = 'Campo campo unidade é obrigatório';
    }

    if (Validator.isEmpty(data.codeitem)) {
        errors.codeitem = 'Campo campo código do item é obrigatório';
    }

    /*if(Validator.isNumeric(data.qtdstock)) {
        errors.qtdstock= 'Campo aceita somente números'
    }*/


    return {
        errors,
        isValid: isEmpty(errors)
    }
}