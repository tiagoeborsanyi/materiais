const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ItemSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    codeitem: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    brand: {
        type: String,
        required: true
    },
    specification: {
        type: String,
        required: true
    },
    referencefabricante: {
        type: String
    },
    unit: {
        type: String,
        required: true
    },
    specificationunit: {
        type: String,
        required: true
    },
    qtdstock: {
        type: Number,
        required: true
    },
    increase: [
        {
            increaseuser: {
                type: Schema.Types.ObjectId,
                ref: 'users'
            },
            increaseqtd: {
                type: Number,
                required: true
            },
            increasedate: {
                type: Date,
                default: Date.now
            }
        }
    ],
    itemobs: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = User = mongoose.model('itens', ItemSchema);