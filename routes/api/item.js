const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Load Validate
const validateItensInput = require('../../validation/item');

// Load Item model
const Item = require('../../models/Item');
// Load User Material
const User = require('../../models/User');

// @route   GET api/itens/test
// @desc    Tests users route
// @access  Public
router.get('/test', (req, res) => res.json({msg: 'Itens works.'}));


// @route   POST api/itens
// @desc    create item
// @access  Private
router.post('/', passport.authenticate('jwt', {session: false}), (req, res) => {
	const { errors, isValid } = validateItensInput(req.body);

	// check validation
	if(!isValid) {
		// If any errors, send 400 with errors object
		return res.status(400).json(errors)
    }

    const qtd = Number.parseInt(req.body.qtdstock);
    if (Number.isNaN(qtd)) {
        errors.quantidade = 'Deve conter somente numeros no campo';
        return res.status(400).json(errors)
    }

	const newItem = new Item({
        user: req.user.id,
        codeitem: req.body.codeitem,
        name: req.body.name,
        image: req.body.image,
        brand: req.body.brand,
        specification: req.body.specification,
        referencefabricante: req.body.referencefabricante,
        unit: req.body.unit,
        specificationunit: req.body.specificationunit,
        qtdstock: req.body.qtdstock,
        itemobs: req.body.itemobs
	});
    Item
        .findOne({ codeitem: req.body.codeitem})
        .then(item => {
            if (item) {
                Item.findOneAndUpdate(
                    { codeitem: req.body.codeitem },
                    { $set: newItem },
                    { new: true}
                ).the(item => res.json(item));
            } else {
                newItem.save().then(item => res.json(item));
            }
        })
});


module.exports = router;