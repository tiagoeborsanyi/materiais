/*
    Documento para fazer update, buscar todos usuários, buscar somente um usuario, 
    coceder permissões de acesso. Usando a coleção User
*/
const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Load Input Validation
const validateProfileInput = require('../../validation/profile');

// Load User model
const User = require('../../models/User');

// @route   GET api/profile/test
// @desc    Tests users route
// @access  Public
router.get('/test', (req, res) => res.json({msg: 'Profile works.'}));


// @route   POST api/profile/update
// @desc    update user
// @access  Private
router.post('/update', (req, res) => {
            
    const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        avatar: req.body.avatar
    });
    
});


module.exports = router;